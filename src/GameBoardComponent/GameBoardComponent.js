import './GameBoardComponent.css';
import ButtonComponent from "../ButtonComponent/ButtonComponent";
import { useEffect, useState } from "react";
import InputComponent from "../InputComponent/InputComponent";

function GameBoardComponent({ gameService, playerService }) {
    const [ game, setGame ] = useState(gameService);
    const [ player, setPlayer ] = useState(playerService);

    function drawPlayers() {
        return game.players.map(player => <div key={player.id} className="playerSeat">{player.name}</div>)
    }

    function addPlayer(evt) {
        evt.preventDefault();
        let data = new FormData(evt.target);
        console.debug('GameBoard form data:', data);
        let name = data.get('playerName');
        console.log(game);
        game.addNewPlayer(name);
    }

    const players = drawPlayers();

    return (
        <div className="gameBoard">
            <h1>GameBoardComponent lives</h1>
            { players }

            <div className="controls">
                <InputComponent handler={addPlayer} player={player} />
                <ButtonComponent buttonName='Start' handler={game.start} />
                <ButtonComponent buttonName='Bid' handler={game.getBids} />
                <ButtonComponent buttonName='Fold' handler={player.fold} />
            </div>
        </div>
    );
}

export default GameBoardComponent;
