import {render, screen} from '@testing-library/react';
import GameBoardComponent from './GameBoardComponent';

test('renders component', () => {
    render(<GameBoardComponent/>);
    const linkEl = screen.getByText(/GameBoardComponent/i);
    expect(linkEl).toBeInTheDocument();
});
