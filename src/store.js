import Game from "./models/Game";

let gameData = JSON.parse(localStorage.getItem('gameData'));
const newGame = new Game();
// Default is blank player for a new game.
// Player uses 'Join' to set the name on this player at game start.
const defaultPlayer = newGame.addNewPlayer('Human');
defaultPlayer.name = 'You';

const store = {
    player: {
        getPlayer: () => {
            if (gameData)
                return gameData.player ? gameData.player : defaultPlayer;
        },
        updatePlayer: (updatedPlayer) => {
            gameData.player = updatedPlayer;
        }},
    game: {
        create: () => {
            let newGame = new Game();
            newGame.setUp();
            newGame.addNewPlayer(defaultPlayer);
            let newData = JSON.stringify({game: newGame, player: defaultPlayer});
            localStorage.setItem('gameData', newData);
            console.log('New game created.', newData)
        },
        get: () => {
            if (!gameData) {
                console.log('No game found. Creating new game.')
                // FIXME: Problem with 'this' again.
                // this.create();
                let newGame = new Game();
                newGame.setUp();
                newGame.addNewPlayer('Default');
                let newData = JSON.stringify({game: newGame, player: defaultPlayer});
                localStorage.setItem('gameData', newData);
                console.log('New game created.', newData)
                gameData = JSON.parse(localStorage.getItem('gameData'));
            }
            return gameData.game;
        },
        reset: () => {
            this.create();
        }
    }
};

export default store;
