import './ButtonComponent.css';

function ButtonComponent(props) {

    function handleClick(evt) {
        // trigger click event passed to the button
        props.handler();
    }

    return (
        <button className='gameButton'
                onClick={handleClick}>

            { props.buttonName }
        </button>
    );
}

export default ButtonComponent;
