class Player {
    constructor(playerName) {
        this.id =  Math.floor(Math.random() * 1000 + 1); // TODO: Testing only, remove before prod
        this.name = playerName;
        this.cards = [];
        this.chips = {'10': 5, '25': 4, '50': 2, '100': 1};
        this.bid = 0;
        this.hasFolded = false;
    }
}


export default Player;
