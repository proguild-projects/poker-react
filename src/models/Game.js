import Player from "./Player";
import Card from "./Card";

class Game {
    constructor() {
        this.currentRound = 1;
        this.highestBid = 0;
        this.deck = [];
        this.players = []
        this.active = false;
    }

    setUp() {
        this.deck = this.createDeck();
        this.currentRound = 1;
        this.addNewPlayer('CPU');
    }

    addNewPlayer(playerName) {
        let player = new Player(playerName);
        this.players.push(player)
        return player
    }

    createDeck() {
        let cards = []
        let suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        let values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King', 'Ace']

        for (let suit of suits) {
            for (let val of values) {
                let card = new Card(suit, val)
                cards.push(card)
            }
        }
        return cards
    }

    dealCards() {
        for (const player of this.players) {
            player.cards.push(this.deck.pop())
            player.cards.push(this.deck.pop())
        }
    }

    drawCards(amount) {
        let cards = []
        for (let i=0; i < amount; i++) {
            cards.push(this.deck.pop())
        }
        return cards;
    }

    getBids() {
        // ask each player if they want to bid.
        // players who don't bid automatically fold and don't go to next round.
        return this.players.map((player) => player.bid)

    }

    getBidders() {
        // return all players who haven't folded.
        return this.players.filter((player) => !player.hasFolded)
    }

    getWinner() {
        // check all players for the one with the best cards and return them.
    }

    start() {
        this.active = true;

        while (this.active) {
            let bids = this.getBids();
            let bidders = this.getBidders();
            if (bidders.length <= 1) {
                let winner = this.getWinner()
                this.active = false
                console.log(`No bidders! Game over.`)
            } else {
                this.currentRound += 1;
            }
        }
        // getWinner()
        // distributeWinnings()

        console.log(`Winner is _______! Game over.`)
    }
}

export default Game;
