/**
 * @summary
 * Flush: all of 1 suit
 * Straight: consecutive cards
 * Full house: 3 of a kind + 1 pair
 *
 * @constructor
 * @param cards {Array<Card>} : The players hand.
 */
class PlayerHand {
    constructor(cards) {
        this.cards = cards;
        this.rank;
        this.combos = []
        this.cardSuits = {'spades': 0, 'hearts': 0, 'clubs': 0, 'diamonds': 0}
        this.cardValues = {
            '2': 0, '3': 0, '4': 0,
            '5': 0, '6': 0, '7': 0,
            '8': 0, '9': 0, '10': 0,
            'Jack': 0, 'Queen': 0, 'King': 0,
            'Ace': 0
        }
        this.flushSet = new Set('Jack', 'Queen', 'King', 'Ace');
    }

    addCard(card) {
        this.cardSuits[card.suit] += 1;
        this.cardValues[card.value] += 1;
        this.cards.push(card)
    }

    getPairs(card) {
        for (const key in this.cardValues) {


            if (this.cardValues[key] === 4) {
                this.combos.push('4 of a kind')
            } else if (this.cardValues[key] === 3) {
                this.combos.push('3 of a kind')
            } else if (this.cardValues[key] === 2) {
                this.combos.push('1 pair')
            }
        }

        if (this.combos && this.flushSet.has(key)) {
            this.combos.push('royal pair')
        }

    }

    /**
     * Rank: Straight
     * @summary 5 consecutive cards, any suit(s)
     */
    hasStraight() {
        // index start: 0, end: 4
        // add +1 on each loop
        // 9 iterations total
        for (let i = 0; i < this.cardValues.length -4; i++) {
            // 5 consecutive cards
            let substr = Object.keys(this.cardValues).slice(i, 4+i);
            // 1 of each
            let result = substr.filter(v => v === 1)
            // if there are 5 in this group, we have a straight
            if (result.length === 5) {
                this.combos.push('straight');
                return true
            }
        }
        return false
    }

    /**
     * Rank: Flush
     * @summary Any 5 cards of the same suit
     */
    hasFlush() {
        for (const suit in this.cardSuits) {
            if (this.cardSuits[suit] === 5) {
                this.combos.push('flush')
                return true
            }
        }
        return false
    }

}
