class Card {
    constructor(suit, value) {
        this.suit = suit;
        this._value = value;
    }

    get name() {
        return `${this._value} of ${this.suit}`;
    }
}


export default Card;
