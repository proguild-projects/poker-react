import './GameComponent.css';
import { useState } from "react";
import GameBoardComponent from "../GameBoardComponent/GameBoardComponent";

function GameComponent({ gameService, playerService }) {
    const { game, setGame } = useState(gameService);
    const { player, setPlayer } = useState(playerService);

    function setUp() {
        // TODO: Might be ok to keep in Game class -- not needed once it exists.
        game.deck = this.createDeck();
        game.currentRound = 1;
        game.addNewPlayer('CPU');
    }

    function getBids() {
        game.getBids()
    }

    function fold() {
        player.fold = true;
        setPlayer({...player, hasFolded: true})
    }

    function addPlayer(evt) {
        evt.preventDefault();
        let data = new FormData(evt.target);
        console.debug('GameBoard form data:', data);
        let name = data.get('playerName');
        console.log(game);
        game.addNewPlayer(name);
    }

    function dealCards() {
        for (const player of this.players) {
            player.cards.push(this.deck.pop())
            player.cards.push(this.deck.pop())
        }
        setPlayer({...player})
    }

    function drawCards(amount) {
        let cards = []
        for (let i=0; i < amount; i++) {
            cards.push(this.deck.pop())
        }
        return cards;
    }

    function getBids() {
        // players who don't bid automatically fold and don't go to next round.
        return this.players
            .map((player) => player.bid)
            .filter(bid => bid > 0)
    }

    function allBidsIn() {
        // return all players who haven't folded.
        return this.players.filter((player) => !player.hasFolded)
    }

    function getWinner() {
        // check all players for the one with the winning hand and return them.
    }

    function play() {
        game.active = true;
        setGame({...game});
        // TODO: When all bids and folds for a round are done, auto compute results in real time.

        // If only 1 or no bidders, winner is known.
        if (getBids().length <= 1) {
            let winner = getWinner()
            this.active = false
            console.log(`No bidders! Game over.`)
        }
        if (allBidsIn()) {
            this.currentRound += 1;
        }

        // getWinner()
        // distributeWinnings()
        console.log(`Winner is _______! Game over.`)
    }

    return (
        <div className="game">
            <h1>GameComponent</h1>
            <GameBoardComponent gameService={gameService} playerService={playerService} />
        </div>
    );
}

export default GameComponent;
