import { render, screen } from '@testing-library/react';
import GameComponent from './GameComponent';

test('renders component', () => {
    render(<GameComponent/>);
    const linkEl = screen.getByText(/GameComponent/i);
    expect(linkEl).toBeInTheDocument();
});
