import './App.css';
import GameBoardComponent from "./GameBoardComponent/GameBoardComponent";
import { useEffect, useState } from "react";
import store from "./store";
import GameComponent from "./GameComponent/GameComponent";

function App() {
    const [game, setGame] = useState(store.game.get());
    const [player, setPlayer] = useState(store.player.getPlayer());

  return (
    <div className="App">
      <h1>Poker World</h1>
        <GameComponent gameService={game} playerService={player} />
        {/*<GameBoardComponent gameService={game} playerService={player} />*/}
    </div>
  );
}

export default App;
