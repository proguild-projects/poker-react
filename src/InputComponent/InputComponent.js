import './InputComponent.css';
import ButtonComponent from "../ButtonComponent/ButtonComponent";
import { useEffect, useState } from "react";


function InputComponent({handler, newPlayer}) {
    const [ inputValue, setValue ] = useState('');
    const [ player, setPlayer ] = useState(newPlayer);

    function addPlayer(evt) {
        console.log('Input event', evt.target.input.value);
        evt.preventDefault();
        handler(evt);
        setPlayer({...newPlayer, name: evt.target.value})
        setValue('');
    }

    return (
        <form className='inputComponent' onSubmit={handler}>
            <input type="text"
                   value={newPlayer}
                   onChange={evt => setValue(evt.target.value)}
                   name="playerName"/>
            <button type="submit"
                    className="gameButton">
                Join
            </button>
        </form>
    );
}

export default InputComponent;
