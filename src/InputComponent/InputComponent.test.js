/**
 Testing Tutorial:
 https://testing-library.com/docs/react-testing-library/example-intro
 */

import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import {InputComponent} from './InputComponent'

test('loads and displays app', async () => {
    // ARRANGE

    // ACT
    await userEvent.click(screen.getByText('My App'))
    await screen.findByRole('heading')

    // ASSERT
    expect(screen.getByRole('heading')).toHaveTextContent('hello world')
})

